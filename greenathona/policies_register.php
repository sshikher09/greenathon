<html>
    <head>
        <title>Minor Project</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >
        <!--jQuery library--> 
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!--Latest compiled and minified JavaScript--> 
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <style>
            .top_margin{
                margin-top:50px;
            }
            .image{
                background-image :url("27.jpg");
                background-repeat:no-repeat;
                background-size:100%;
                background-position-y:380px;
                background-color:RGB(95,122,143);
            }
        </style>
    </head>
</head>
<body class="image">
    <?php
        include 'header.php';
        ?>
        <div class="container1">
            <div class="row">
               <div class="row" style="font-size:50px">
                <center><b>
                         Tourist Spots Recommendation </b>
                </center>
            </div>
            </div>
        </div>

<div class="container">
        <div class="row top_margin">
            <div class="col-xs-8 col-xs-offset-2">
                <div class="panel panel-primary">
                    <div class="panel-heading">Please fill details regarding the place below</div>
                    <div class="panel-body">
                        <form method="POST" action="place_register_script.php">
                            <div class="form-group">
                                <label for="pid">Place Id</label>
                                <input type="number" class="form-control" autocomplete="off" placeholder="ALLOCATES AUTOMATICALLY" id="pid" name="pid" disabled="true">
                            </div>
                            <div class="form-group">
                                <label for="name">Name Of the Place</label>
                                <input type="text" class="form-control" placeholder="Name Of Tourist Place" autocomplete="off" id="name" name="name" required="true">
                            </div>
                            <div class="form-group">
                                <label for="timings">Timings</label>
                                <input type="text" class="form-control" autocomplete="off" id="timings" name="timings" required="true">
                            </div>
                            <div class="form-group ">
                                <label for="attractions">Please Enter Major Attractions of the place</label><br>
                                <input type="text"  class="form-control" id="attractions" name="attractions" required="true" >
                               </div>
                            <div class="form-group">
                                <label for="price">Entry Fees</label>
                                <input type="text" class="form-control" autocomplete="off" id="price" name="price" required="true">
                            </div>
                          
                            <button type="submit" class="btn btn-primary" value="registration_submit">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</body>
</html>

