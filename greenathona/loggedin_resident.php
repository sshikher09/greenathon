 <?php session_start();?>
<html>
    <head>
        <title>Greenathon</title>
        <link href="https://fonts.googleapis.com/css?family=Comfortaa" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >
        <!--jQuery library--> 
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!--Latest compiled and minified JavaScript--> 
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <style>
            .background{background-image :url("tyu.jpg");
                background-repeat: no-repeat;
                background-size: cover;
                font-family: 'Comfortaa', cursive;
                color:brown;}
        
            .top_margin{
                margin-top:55px;
            }
            #content{ padding:10px;
          background-color:rgba(255,255,255,0.7);
          max-width:250px;
         
}

        </style>
    </head>
</head>
<body class='background'>
    <?php
        include 'header2.php';
        ?>
        <div class='top_margin'>
            <div class="container1" style="font-size:50px">
                <div class="row">
                    <div class="col-xs-4 col-xs-offset-4">
                
                        <center><h1><u><b>Successfully Logged In</b></u></h1><center><br><br>
                    </div>
                </div>
            </div>
        
            <div class="row">
       
                    <br>
                    <br><br><br>
                    <center><div style="margin-top:50px">
                    <center><h4>To get started<a href="getstarted.php" style="color:blue"> Click Here</a> </h4>
            </center>
                    <center>
                    <h4>If you want to know more about us, you can do it <a href="contactus.php" style="color:blue">Here</a>  </h4>
                    </center>
                    </div></center>
    
            </div>
            
</body>
</html>