<?php
$con = mysqli_connect("localhost", "root", "shaurya", "greenathon")or die($mysqli_error($con));
$select_query = "SELECT  pid , policy_name , benefit , requirements , valid_till FROM policies";
$select_query_result = mysqli_query($con, $select_query) or die(mysqli_error($con));
?>

<html>
    <head>
        <title>greenathon</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >
        <!--jQuery library--> 
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!--Latest compiled and minified JavaScript--> 
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <style>
            .top_margin{
                margin-top:50px;
            }
             .image{
                background-image :url("fds.jpg");
                background-repeat:no-repeat;
                background-size:cover;
                
            }
            .content{ 
                         background-color:rgba(255,255,255,0.6);
                         max-width: 500px;
         }
        </style>
    </head>
</head>
<body class="image">
    <?php
        include 'header.php';
        ?>
        <div class="container1">
            <div class="row">
                  <div <div class="row" style="font-size:40px ; color:white;">
                <center><b>
                       POLICIES BY GOVT OF RAJASTHAN UNDER SMART WASTE WATER MANAGEMENT PLAN</b><br><br>
                </center>
            </div>
        </div>
<div class="container">
        <?php while ($row = mysqli_fetch_array($select_query_result)) { ?>
          <div class="content">
              <br>
          <div class="row">
              <div class="col-xs-5 col-xs-offset-1">Policy Id</div>
              <div class="col-xs-15"><?php echo $row['pid']; ?></div>
          </div>
         <div class="row">
              <div class="col-xs-5 col-xs-offset-1">Policy Name</div>
              <div class="col-xs-15"><?php echo $row['policy_name']; ?></div>
          </div>
         <div class="row">
              <div class="col-xs-5 col-xs-offset-1">Benefits under this policy</div>
              <div class="col-xs-15"><?php echo $row['benefit']; ?></div>
          </div>
          <div class="row">
              <div class="col-xs-5 col-xs-offset-1">Requirement to avail benefit</div>
              <div class="col-xs-15"><?php echo $row['requirements']; ?></div>
          </div>
        <div class="row">
              <div class="col-xs-5 col-xs-offset-1">Policy is valid till </div>
              <div class="col-xs-15"><?php echo $row['valid_till']; ?></div>
          </div>
              <br>
          </div>
		  <hr/>
        <?php } ?>
    </div>
</body>
</html>