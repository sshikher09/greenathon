-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 20, 2018 at 10:13 PM
-- Server version: 5.7.17-log
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `greenathon`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `name` varchar(20) NOT NULL,
  `adminid` int(10) NOT NULL,
  `password` varchar(50) NOT NULL,
  `username` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`name`, `adminid`, `password`, `username`) VALUES
('Shaurya Shikher', 1001, 'cf8e4a082209e6cee64ed36d8b9e0c5d', 'sshikher09');

-- --------------------------------------------------------

--
-- Table structure for table `policies`
--

CREATE TABLE `policies` (
  `pid` int(5) NOT NULL,
  `policy_name` varchar(30) NOT NULL,
  `benefit` varchar(50) NOT NULL,
  `requirements` varchar(50) NOT NULL,
  `valid_till` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `policies`
--

INSERT INTO `policies` (`pid`, `policy_name`, `benefit`, `requirements`, `valid_till`) VALUES
(1231, 'pani bachao desh bachao', 'tax benefit upto Rs 1000 per month', 'water score >= 150', 'jan 2019');

-- --------------------------------------------------------

--
-- Table structure for table `resident`
--

CREATE TABLE `resident` (
  `rid` int(4) NOT NULL,
  `name` varchar(20) NOT NULL,
  `emailid` varchar(40) NOT NULL,
  `mobileno` bigint(10) NOT NULL,
  `age` int(10) NOT NULL,
  `gender` enum('Male','Female','Others') NOT NULL,
  `username` varchar(20) NOT NULL,
  `city` varchar(20) NOT NULL,
  `district` varchar(20) NOT NULL,
  `password` varchar(50) NOT NULL,
  `locality` varchar(30) NOT NULL,
  `no_of_members` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `resident`
--

INSERT INTO `resident` (`rid`, `name`, `emailid`, `mobileno`, `age`, `gender`, `username`, `city`, `district`, `password`, `locality`, `no_of_members`) VALUES
(1001, 'shaurya', 'sshikher09@gmail.com', 7073559466, 20, 'Male', 'sshikher09', 'jaipur', 'jaipur', 'cf8e4a082209e6cee64ed36d8b9e0c5d', 'dehmi kalan', 7),
(1014, 'Saamarth', 'saamarthrastogi@hotmail.com', 7073827134, 21, 'Male', 'saamrastogi', 'jaipur', 'dhemi kalan', '49ddceb5cb5f16d76e3d8f7b4016f0f5', 'ncghfhfh', 5);

-- --------------------------------------------------------

--
-- Table structure for table `waterwastagestatistics`
--

CREATE TABLE `waterwastagestatistics` (
  `mid` int(5) NOT NULL,
  `name_of_month` varchar(30) NOT NULL,
  `water_wastage_quantity` varchar(30) NOT NULL,
  `credit_score` varchar(20) NOT NULL,
  `year` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`adminid`);

--
-- Indexes for table `policies`
--
ALTER TABLE `policies`
  ADD PRIMARY KEY (`pid`);

--
-- Indexes for table `resident`
--
ALTER TABLE `resident`
  ADD PRIMARY KEY (`rid`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `waterwastagestatistics`
--
ALTER TABLE `waterwastagestatistics`
  ADD PRIMARY KEY (`mid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `adminid` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1002;
--
-- AUTO_INCREMENT for table `policies`
--
ALTER TABLE `policies`
  MODIFY `pid` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1232;
--
-- AUTO_INCREMENT for table `resident`
--
ALTER TABLE `resident`
  MODIFY `rid` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1015;
--
-- AUTO_INCREMENT for table `waterwastagestatistics`
--
ALTER TABLE `waterwastagestatistics`
  MODIFY `mid` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1007;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
