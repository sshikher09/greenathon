
<html>
    <head>
        <title>Greenathon</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >
        <!--jQuery library--> 
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!--Latest compiled and minified JavaScript--> 
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <style>
            .image{ background-image :url(" jko.jpg");
              background-repeat:no-repeat;
            }
            .top_margin{
                margin-top:50px;
            }
        </style>
    </head>
</head>
<body class="image">
    <?php
        include 'header.php';
        ?>
        <div class="container1">
            <div class="row">
                
                <center> <div style="color:white;font-size:50px"><b>RESIDENT REGISTRATION PAGE</b>
                </center> </div>
            </div>
        </div>

<div class="container">
        <div class="row top_margin">
            <div class="col-xs-8 col-xs-offset-2">
                <div class="panel panel-info">
                    <div class="panel-heading"><h3>Resident Registration</h3></div>
                    <div class="panel-body">
                        <form method="POST" action="resident_registration_script.php">
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" autocomplete="off" placeholder="Name" id="name" name="name" required="true">
                            </div>
							<div class="form-group">
                                <label for="emailid">Email ID</label>
                                <input type="text" class="form-control" placeholder="Email Id" id="emailid" name="emailid" required="true">
                            </div>
							<div class="form-group">
                                <label for="mobileno">Contact Number</label>
                                <input type="number" class="form-control" placeholder="Mobile Number" id="mobileno" name="mobileno" maxlength="10" required="true">
                            </div>
							<div class="form-group">
                                <label for="age">Age</label>
                                <input type="number" class="form-control" id="age" name="age" required="true">
                            </div>
							<div class="form-group">
                                <label for="gender">Gender</label>
                                <select class="form-control" placeholder="Select" id="gender" name="gender" required="true">
								<option value='Male'>Male</option>   
                                <option value='Female'>Female</option>
                                <option value='Others'>Others</option>
                                </select>								
                            </div>
                            <div class="form-group">
                                <label for="username">User Name</label>
                                <input type="text" class="form-control" placeholder="User Name" autocomplete="off" id="username" name="username" required="true">
                            </div>
							<div class="form-group">
                                <label for="city">city</label>
                                <input type="text" class="form-control" id="city" name="city" required="true">
                            </div>
							<div class="form-group">
                                <label for="district">district</label>
                                <input type="text" class="form-control" id="district" name="district" required="true">
                            </div>
                            <div class="form-group">
                                <label for="password">Password</label>
                                <input type="password" class="form-control" placeholder="Please Include (UpperCase, LowerCase, Number/SpecialChar and min 8 Chars)" id="password" name="password" required="true" pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$">
                            </div>
                        <div class="form-group">
                                <label for="locality">Locality</label>
                                <input type="text" class="form-control" id="locality" name="locality" required="true">
                            </div>
                            <div class="form-group">
                                <label for="no_of_members">No of members in your family</label>
                                <input type="text" class="form-control" id="no_of_members" name="no_of_members" required="true">
                            </div>

						<button type="submit" class="btn btn-primary" value="registration_submit">Proceed</button>
                        
						</form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</body>
</html>
