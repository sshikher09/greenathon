<html>
    <head>
        <title>Portal</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >
        <!--jQuery library--> 
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!--Latest compiled and minified JavaScript--> 
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <style>
            .image{
            background-image :url("17.jpg");

background-repeat:no-repeat;



            }
            .top_margin{
                margin-top:50px;
            }
        </style>
    </head>
</head>
<body class="image">
     <?php
        include 'header.php';
        ?>
<div class="container">
        <div class="row top_margin">
            <div class="col-xs-8 col-xs-offset-2">
                <div class="panel panel-primary">
                    <div class="panel-heading"style="background-color:black">Log In Page</div>
                    <div class="panel-body">
                        <form method="POST" action="admin_login_script.php">                        
                            <div class="form-group">
                                <label for="username">User Name</label>
                                <input type="text" class="form-control" placeholder="User Name" autocomplete="off" id="username" name="username" required="true">
                            </div>
                            <div class="form-group">
                                <label for="password">Password</label>
                                <input type="password" class="form-control" placeholder="Please include upperCase, lowerCase, number/specialchar and min 8 Chars" id="password" name="password" required="true" pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$">
              
                            
                            </div>
                            
                            <button type="submit" class="btn btn-primary" value="registration_submit" style="background-color:black">Submit</button>
                        </form>
                    </div>
                    </div>
    
         </div>
    </div>
</body>