<html>
    <head>
        <title>Greenathon</title>
        <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >
        <!--jQuery library--> 
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!--Latest compiled and minified JavaScript--> 
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <style>
            .image{
            background-image :url("rawat5.jpg");
background-size:cover;
background-repeat:no-repeat;
font-family: 'Montserrat', sans-serif;


            }
            .top_margin{
                margin-top:50px;
                
            }
        </style>
    </head>
</head>
<body class="image">
     <?php
        include 'header.php';
        ?>
   <div class="container">
        <div class="row top_margin">
            <div class="col-xs-8 col-xs-offset-2">
                <div class="panel panel-danger" style="background-color:rgb(61, 64, 68)">
                    <div class="panel-heading" style="color:black"><center><h3>Login Page</h3></center></div>
                    <div class="panel-body">
                        <form method="POST" action="resident_login_script.php">                        
                           <div class="form-group">
                                <label for="username" style="color:white">User Id</label>
                                <input type="text" class="form-control" placeholder="User Name" autocomplete="off" id="username" name="username" required="true">
                            </div>
                            <div class="form-group">
                                <label for="password" style="color:white">Password</label>
                                <input type="password" class="form-control" placeholder="Please Include ( upperCase, lowercase, specialchar/num and min 8 Chars)" id="password" name="password" required="true" pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$">
                                <a href="password_change_resident.php" style="color:red">Forget password</a>
                            </div>
                            
                            <button type="submit" class="btn btn-primary" value="registration_submit">Submit</button>
                        </form>
                    </div>
                </div>
                <div style="color:white">Not a member yet <a href ="resident_registration.php"style="color:blue">Click Here</a></div>
        </div>
    </div>
  
</body>